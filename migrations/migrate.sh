#!/bin/bash

if [ ! -f holger.db ]
then
   echo "Cannot find 'holger.db' in this directory, creating one.";
   echo "" | sqlite3 holger.db;
   echo "PRAGMA schema.user_version = 0" | sqlite3 holger.db;
fi;

echo "Applying migrations..."

for f in `ls migrations/*.sql | grep "^migrations/...\.sql" | sort -V`;
do
    f=$(basename -- "$f");
    mig_version=${f%.*}
    db_version=$(echo "pragma user_version;" | sqlite3 holger.db);
    if [ $db_version -le $mig_version ]
    then
	echo "Applying $f";
	sqlite3 holger.db < migrations/$f || exit 1;
	echo "pragma user_version=$mig_version" | sqlite3 holger.db;
    fi;
done;

echo "";
echo "Done migrating holger.db";
