TARGET_NAME=holger
CPP=clang++
CPPFLAGS=-std=c++17 -g3 -pedantic -Wall -Wextra -Werror -Wpedantic -Wgcc-compat -Wshadow -Wunreachable-code -Weffc++ -O0
INCLUDES=-I/usr/local/include
LDFLAGS=-std=c++17
LDLIBS=-L/usr/local/lib -lwt -lwthttp -lwtdbo -lwtdbosqlite3 -lboost_system -lboost_thread -lboost_filesystem -lboost_program_options

SRC_DIR := src
OBJ_DIR := build
SRC_FILES := $(wildcard $(SRC_DIR)/*.cpp)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_FILES))

all: $(TARGET_NAME)

$(TARGET_NAME): $(OBJ_FILES)
	@echo "Linking..."
	@$(CPP) $(LDFLAGS) $(LDLIBS) -o $@ $^

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@echo "Compiling $<"
	@$(CPP) $(CPPFLAGS) $(INCLUDES) -c -o $@ $<

clean:
	rm build/*.o
	rm $(TARGET_NAME)

rmdb:
	rm holger.db

holger.db:
	@migrations/migrate.sh	

migrate: holger.db
	@echo "Running migrations..."

run: $(TARGET_NAME) holger.db
	@echo "Starting webserver on localhost:8080"
	@LD_LIBRARY_PATH=/usr/local/lib ./$(TARGET_NAME) --docroot "docroot" --http-address 127.0.0.1 --http-port 8080

