Just Another Holgerspexet At Punkt se 

# Dependencies

This project depends on boost, Wt and sqlite3, with an additional
build dependency on CMake.

## Ubuntu 18.04

```
sudo apt-get install gcc g++ libboost-all-dev cmake
git clone https://github.com/emweb/wt.git
cd wt
git checkout 4.0.3
mkdir build && cd build && cmake .. && make
sudo make install # Or `cp $(find | grep "*.so") $SOME_LD_PATH` and change Makefile
```

## Others

https://redmine.webtoolkit.eu/projects/wt/wiki/Wt_Installation


# Development

All code should be modern C++ up to C++17. Clang is used and all code
should compile without warnings.

# Running

In the project directory, run `make run`.  Please see `Makefile` for
some more fun stuff.

# Database

Database migrations are solved by the ugly shell script in
`migrations/migrate.sh`, the `Makefile` should handle most of the
interactions here. Ghetto migrations for the win.

# Comments and documentation

Naah. Just write readable, straightforward code, without being clever.

# Code style

Naah. Just write readable, straightforward code, without being clever.
