#ifndef CONTENTDATA_H
#define CONTENTDATA_H

#include <Wt/Dbo/Dbo.h>
#include <string>

#include "helpers.h"
#include "Session.h"

using namespace std;

class ContentData {
 public:
  string path;
  string data;
  template<class Action>
  void persist(Action& a) {
    dbo::field(a, data, "data");
    dbo::field(a, path, "path");
  }
private:
};

namespace Content {
  dbo::ptr<ContentData>
  findByPath(Session s, const string & path);

  dbo::ptr<ContentData>
  create(Session s, const string & path, const string & data);

  void
  update(Session s, dbo::ptr<ContentData> content, const string & data);
}


#endif /* CONTENTDATA_H */
