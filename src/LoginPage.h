#ifndef LOGINPAGE_H
#define LOGINPAGE_H

#include <Wt/WContainerWidget.h>

#include "Session.h"
#include "helpers.h"

class LoginPage : public Wt::WContainerWidget
{
public:
  LoginPage(Session s);

private:
  Session s_;
};

#endif /* LOGINPAGE_H */
