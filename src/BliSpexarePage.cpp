#include <Wt/WStackedWidget.h>
#include <Wt/WNavigationBar.h>
#include <Wt/WBorderLayout.h>
#include <Wt/WGroupBox.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <Wt/WMenu.h>

#include "BliSpexarePage.h"
#include "helpers.h"
#include "ContentWidget.h"

using namespace Wt;

BliSpexarePage::BliSpexarePage(Session s)
  : s_(s)
{
  auto layout = setLayout(make_unique<WBorderLayout>());
  layout->addWidget(make_unique<WText>("Right"), LayoutPosition::East);
  layout->addWidget(make_unique<WText>("Left"), LayoutPosition::West);
  layout->addWidget(make_unique<WText>("North"), LayoutPosition::North);

  auto groupBox = make_unique<WGroupBox>("Bli Spexare!");
  groupBox->addWidget(make_unique<ContentWidget>(s_,"blispexare/intro"));

  layout->addWidget(move(groupBox), LayoutPosition::Center);
}
