#ifndef CONTENTWIDGET_H
#define CONTENTWIDGET_H

#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <string>

#include "Session.h"
#include "helpers.h"
#include "ContentData.h"

using namespace std;

class ContentWidget : public Wt::WContainerWidget
{
public:
  ContentWidget(Session s, const string & path);

private:
  Session s_;
  dbo::ptr<ContentData> content_;
};

#endif /* CONTENTWIDGET_H */
