#ifndef SESSION_H
#define SESSION_H

#include <Wt/Dbo/Dbo.h>
#include <Wt/Auth/AuthService.h>
#include <Wt/Auth/Login.h>
#include <Wt/Auth/PasswordService.h>
#include <Wt/Auth/Dbo/UserDatabase.h>

#include "UserData.h"
#include "helpers.h"


// A new namespace just to get the names right.
namespace sessionimpl {
  class Session {
  public:
    // Make it exceedingly hard to create a new session
    Session() = delete;
    Session(const Session&) = delete;
    Session(std::string); // Provide a foo string to create it.


    dbo::Session db;
    Wt::Auth::AuthService& auth();
    Wt::Auth::PasswordService& passwordAuth();
    Wt::Auth::Login& login() { return login_; };
    Wt::Auth::Dbo::UserDatabase<Wt::Auth::Dbo::AuthInfo<UserData>>& users();
  private:
    Wt::Auth::Login login_;
    std::unique_ptr<Wt::Auth::Dbo::UserDatabase<Wt::Auth::Dbo::AuthInfo<UserData>>> user_db_;
  };
}


using Session = std::shared_ptr<sessionimpl::Session>;


#endif /* SESSION_H */
