#include "ContentData.h"

dbo::ptr<ContentData>
Content::findByPath(Session s, const string & path)
{
  dbo::Transaction t {s->db};
  dbo::ptr<ContentData> res = s->db.find<ContentData>().where("path = ?").bind(path);
  if(!res) {
    res = Content::create(s, path, "New content at " + path);
  }
  return res;
}

dbo::ptr<ContentData>
Content::create(Session s, const string & path, const string & data)
{
  dbo::Transaction t {s->db};

   auto content = make_unique<ContentData>();
   content->path = path;
   content->data = data;
   return s->db.add(move(content));
}

void
Content::update(Session s, dbo::ptr<ContentData> content, const string & data)
{
  dbo::Transaction t {s->db};

  content.modify()->data = data;
}
