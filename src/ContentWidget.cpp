#include "ContentWidget.h"

#include <Wt/Utils.h>
#include <Wt/WText.h>
#include <Wt/WTextEdit.h>
#include <Wt/WDialog.h>
#include <Wt/WPushButton.h>
#include <Wt/WMessageBox.h>
#include "helpers.h"
#include "ContentData.h"

using namespace Wt;


ContentWidget::ContentWidget(Session s, const string & path)
: s_(s)
{
  content_ = Content::findByPath(s_, path);

  auto text = addWidget(make_unique<WText>(content_->data, TextFormat::UnsafeXHTML));
  auto edit_button = addWidget(make_unique<WPushButton>("Redigera"));
  edit_button->setStyleClass("btn-link");
  edit_button->clicked().connect([=] {
      auto dialog = addChild(make_unique<WDialog>("Redigera"));
      dialog->resize(800, 640);

      auto editor = dialog->contents()->addWidget(make_unique<WTextEdit>(content_->data));
      editor->setConfigurationSetting("height", 10);
      editor->resize(editor->width(), 470);

      auto save_button = dialog->footer()->addWidget(make_unique<WPushButton>("Spara"));
      save_button->setStyleClass("btn-primary");
      save_button->clicked().connect([=] {
	  Content::update(s_, content_, editor->text().toUTF8());
	  text->setText(editor->text());
	  removeChild(dialog);
	});

      auto cancel_button = dialog
	->footer()
	->addWidget(make_unique<WPushButton>("Avbryt"));
      cancel_button->clicked().connect([=] {
	  removeChild(dialog);
	});
      dialog->show();
  });
}
