#include <iostream>

#include <Wt/WStackedWidget.h>
#include <Wt/WNavigationBar.h>
#include <Wt/WBorderLayout.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <Wt/WMenu.h>

#include "RootPage.h"
#include "BliSpexarePage.h"
#include "LoginPage.h"
#include "ContentWidget.h"
#include "helpers.h"

using namespace Wt;

RootPage::RootPage(Session s)
  : s_(s)
{
  // Create the big navbar at the top
  auto navbar = make_unique<WNavigationBar>();
  navbar->setTitle("Holgerspexet", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");
  navbar->setResponsive(false);

  // The stack will contain all the diffenrent views later on
  auto stack = make_unique<WStackedWidget>();

  // The menu will appear in the navbar and handle navigation between
  // different views
  menu_ = navbar->addMenu(make_unique<WMenu>(stack.get()));

  // Move everything to its right place
  addWidget(move(navbar));
  addWidget(move(stack));

  menu_->addItem("Spex!", make_unique<ContentWidget>(s_, "root/spex"));
  menu_->addItem("Tidigare spex", make_unique<ContentWidget>(s_, "root/tidigare_spex"));
  menu_->addItem("Bli spexare", make_unique<BliSpexarePage>(s_));
  menu_->addItem("Kontakt", make_unique<ContentWidget>(s_,"root/contact"));
  login_page_ = menu_->addItem("Logga in", make_unique<LoginPage>(s_));
  user_page_ = menu_->addItem("Logga in", make_unique<LoginPage>(s_));

  s_->login().changed().connect([=](){ update(); });
  update();
}

void RootPage::update() {
  if (s_->login().loggedIn()) {
    menu_->setItemHidden(login_page_, true);
    menu_->setItemHidden(user_page_, false);
    cout <<  "logged in" << endl;
  } else {
    menu_->setItemHidden(login_page_, false);
    menu_->setItemHidden(user_page_, true);
    cout <<  "logged out" << endl;
  }
}

void
RootPage::createPublicPage() {
  // Add menu items for public parts of the web page
  auto items = menu_->items();
  for (auto i : items) {
    menu_->removeItem(i);
  }

}

void
RootPage::createInternalPage() {
  auto items = menu_->items();
  for (auto i : items) {
    menu_->removeItem(i);
  }
}
