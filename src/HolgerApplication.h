#ifndef HOLGERAPPLICATION_H
#define HOLGERAPPLICATION_H

#include <Wt/WApplication.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WPushButton.h>
#include <Wt/WText.h>


class HolgerApplication : public Wt::WApplication
{
public:
    HolgerApplication(const Wt::WEnvironment& env);

private:
};

#endif /* HOLGERAPPLICATION_H */
