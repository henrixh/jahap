#include <Wt/WStackedWidget.h>
#include <Wt/WNavigationBar.h>
#include <Wt/WBorderLayout.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WText.h>
#include <Wt/WMenu.h>

#include "InternalRootPage.h"
#include "helpers.h"

using namespace Wt;

InternalRootPage::InternalRootPage()
{
  
  // Create the big navbar at the top
  auto navbar = make_unique<WNavigationBar>();
  navbar->setTitle("Holgerspexet");
  navbar->setResponsive(true);

  // The stack will contain all the diffenrent views later on
  auto stack = make_unique<WStackedWidget>();

  // The menu will appear in the navbar and handle navigation between
  // different views
  auto menu = make_unique<WMenu>(stack.get());
  auto menu_ = navbar->addMenu(move(menu));

  menu_->addItem("Spex!", make_unique<WText>("Speeex!"));
  menu_->addItem("Tidigare spex", make_unique<WText>("Tidigare spex :D"));
  menu_->addItem("Bli spexare", make_unique<WText>("Jag kan sluta spexa när jag vill"));
  menu_->addItem("Kontakt", make_unique<WText>("Bara maila som vanliga människor"));

  // Create a root border layout
  auto layout = make_unique<WBorderLayout>();

  addWidget(move(navbar));
  addWidget(move(stack));
}
