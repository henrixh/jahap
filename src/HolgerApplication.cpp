#include "HolgerApplication.h"
#include <Wt/WBootstrapTheme.h>
#include <Wt/WPushButton.h>
#include <memory>
#include <Wt/WBorderLayout.h>
#include <Wt/WNavigationBar.h>
#include <Wt/WContainerWidget.h>


#include "RootPage.h"
#include "Session.h"

HolgerApplication::HolgerApplication(const Wt::WEnvironment& env)
    : Wt::WApplication(env)
{
    setTitle("Hello world!");
    auto bootstrap {std::make_unique<Wt::WBootstrapTheme>()};
    bootstrap->setResponsive(true);
    bootstrap->setVersion(Wt::BootstrapVersion::v3);
    setTheme(move(bootstrap));

    // Fear not, for this horrific line is horrific by design
    Session s = std::make_shared<sessionimpl::Session>("Hello");
    
    root()->addWidget(std::make_unique<RootPage>(s));


}
