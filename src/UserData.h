#ifndef USERDATA_H
#define USERDATA_H

#include <Wt/Dbo/Dbo.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include <string>

#include "helpers.h"

using namespace std;

class UserData;

typedef Wt::Auth::Dbo::AuthInfo<UserData> AuthInfo;
typedef dbo::collection< dbo::ptr<UserData> > Users;

class UserData {
 public:
  // Basic info
  string username;
  string email;
  string firstname;
  string familyname;
  string nickname;

  // Authentication
  dbo::collection<dbo::ptr<AuthInfo>> authInfos;

  template<class Action>
  void persist(Action& a) {
    dbo::field(a, username, "username");
    dbo::field(a, email, "email");
    dbo::field(a, firstname, "firstname");
    dbo::field(a, familyname, "familyname");
    dbo::field(a, nickname, "nickname");
 
    dbo::hasMany(a, authInfos, dbo::ManyToOne, "user");
  }

 private:
};

namespace User {

}


#endif /* USERDATA_H */
