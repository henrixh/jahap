#ifndef INTERNALROOTPAGE_H
#define INTERNALROOTPAGE_H

#include <Wt/WContainerWidget.h>
#include <Wt/WMenu.h>

#include "Session.h"
using namespace Wt;

class RootPage : public Wt::WContainerWidget
{
public:
  RootPage(Session s);
 private:
  void createPublicPage();
  void createInternalPage();
  void update();

  WMenu * menu_;
  Session s_;

  WMenuItem* login_page_;
  WMenuItem* user_page_;
    
};

#endif // INTERNALROOTPAGE_H
