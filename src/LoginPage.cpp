#include "LoginPage.h"

#include <Wt/WText.h>
#include <Wt/Auth/AuthWidget.h>
#include <Wt/WServer.h>

#include "helpers.h"
LoginPage::LoginPage(Session s)
: s_(s)
{
  s_->login().changed().connect([=] () {
      s_->login().loggedIn() ? Wt::log("Logging in") : Wt::log("Logging out");
    });
  
  auto auth = make_unique<Wt::Auth::AuthWidget>(s_->auth(), s_->users(), s_->login());
  auth->model()->addPasswordAuth(&s_->passwordAuth());
  auth->setRegistrationEnabled(true);
  auth->processEnvironment();
  addWidget(move(auth));
}
