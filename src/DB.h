#ifndef DB_H
#define DB_H

#include "Session.h"

using SessionImpl = sessionimpl::Session;

namespace DB {
  void setup(SessionImpl & s);
  namespace internal {
    void connect(SessionImpl & s);
    void mapClasses(SessionImpl & s);
    void initiateSchema(SessionImpl & s);
  };
};

#endif /* DB_H */
