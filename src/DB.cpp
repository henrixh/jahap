#include "DB.h"

#include <Wt/Dbo/Dbo.h>
#include <Wt/Dbo/backend/Sqlite3.h>
#include <Wt/Auth/Dbo/AuthInfo.h>
#include "DB.h"
#include "ContentData.h"
#include "UserData.h"

using AuthInfo = Wt::Auth::Dbo::AuthInfo<UserData>;

namespace DB {
  void setup(SessionImpl & s) {
    internal::connect(s);
    internal::mapClasses(s);
  }

  namespace internal {
    void connect(SessionImpl & s) {
      auto sqlite = make_unique<dbo::backend::Sqlite3>("holger.db");
      s.db.setConnection(move(sqlite));
    }

    void mapClasses(SessionImpl & s) {
      s.db.mapClass<ContentData>("content");
      s.db.mapClass<UserData>("user");
      s.db.mapClass<AuthInfo>("auth_info");
      s.db.mapClass<AuthInfo::AuthIdentityType>("auth_identity");
      s.db.mapClass<AuthInfo::AuthTokenType>("auth_token");
    }
  }
}
