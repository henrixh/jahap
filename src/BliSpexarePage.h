#ifndef BLISPEXAREPAGE_H
#define BLISPEXAREPAGE_H

#include <Wt/WContainerWidget.h>

#include "Session.h"

class BliSpexarePage : public Wt::WContainerWidget
{
public:
    BliSpexarePage(Session s);

private:
  Session s_;
};


#endif /* BLISPEXAREPAGE_H */
