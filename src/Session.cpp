#include "Session.h"

#include <Wt/Dbo/Dbo.h>
#include <Wt/Auth/AuthService.h>
#include <Wt/Auth/HashFunction.h>
#include <Wt/Auth/PasswordService.h>
#include <Wt/Auth/PasswordStrengthValidator.h>
#include <Wt/Auth/PasswordVerifier.h>
#include <Wt/Auth/GoogleService.h>
#include <Wt/Auth/FacebookService.h>

#include "DB.h"
#include "UserData.h"


namespace {
  Wt::Auth::AuthService authService;
  Wt::Auth::PasswordService passwordService{ authService };
}

namespace sessionimpl {
  Session::Session(std::string) {
    DB::setup(*this);

    // Setup authentication

    authService.setAuthTokensEnabled(true, "logincookie");
    authService.setEmailVerificationEnabled(false);
    authService.setEmailVerificationRequired(false);
    
    std::unique_ptr<Wt::Auth::PasswordVerifier> verifier =
	std::make_unique<Wt::Auth::PasswordVerifier>();
    user_db_ = make_unique<Wt::Auth::Dbo::UserDatabase<Wt::Auth::Dbo::AuthInfo<UserData>>>(db);

    verifier->addHashFunction(std::make_unique<Wt::Auth::BCryptHashFunction>(7));
    passwordService.setVerifier(std::move(verifier));
    passwordService.setAttemptThrottlingEnabled(true);
    passwordService.setStrengthValidator(
       std::make_unique<Wt::Auth::PasswordStrengthValidator>());
  }


   Wt::Auth::AuthService&
   Session::auth() {
     return authService;
   }

   Wt::Auth::PasswordService&
   Session::passwordAuth() {
     return passwordService;
   }


   Wt::Auth::Dbo::UserDatabase<Wt::Auth::Dbo::AuthInfo<UserData>>&
   Session::users() { return *user_db_; }

};
